﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace geotexttest.Models
{
    public class proyecto
    {
        public Guid Id { get; set; }
        /*it is used for geo location*/
        [MaxLength(20)]
        [Required]
        public string PostalCode { get; set; }
        [MaxLength(20)]
        [Required]
        public string City { get; set; }
        [MaxLength(20)]
        [Required]
        public string Country { get; set; }
        [MaxLength(50)]
        public string FullAdress { get; set; }
    }
}