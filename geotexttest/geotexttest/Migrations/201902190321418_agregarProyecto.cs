namespace geotexttest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class agregarProyecto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.proyecto",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PostalCode = c.String(maxLength: 20),
                        City = c.String(maxLength: 20),
                        Country = c.String(maxLength: 20),
                        FullAdress = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.proyecto");
        }
    }
}
